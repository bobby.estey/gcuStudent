# SYLLABUS ADDENDUM ← This DOCUMENT overrides everything stated by any other document.

Please read all of this announcement which will help you be successful in this course.  I am your class facilitator:

**Bobby Estey, robert.estey@my.gcu.edu 775.513.7587  Office Hours:  1800 - 2200 Weekdays / 1300 - 2200 Weekends (Arizona Time)**

We will soon be working with some very interesting topics in our course environment.  The University Website has various sets of tools that will provide us with content, examples, reference sources and knowledge.  Click on the Syllabus link which covers the “rules” of the class and is the contract.  
 
## Course Override and Final Authority (AKA Syllabus Addendum)

There are several publications and videos that are in this course and throughout the University.  If anything differentiates from these artifacts, the syllabus addendum is the final authority. 

## Discussion Support

I wanted to inform the class that I do not mind if you do the following, as a matter of fact I encourage your discussions to do the following:

- **Internet** - use the Internet, do not copy the content.  Reference the sites you are using, share with others the web addresses, etc.  This is not cheating or illegal, this is the real world, you have peers at work and friends at home who have questions, what do you do?  Tell them to take hike, NO!!!

- **Assisting Others** - do not wait for me to assist students, I cannot be in the classroom 24x7, however, if you see students with questions, respond to them, assist them.  This is participation and a win-win situation, you get discussion credit and they get immediate help.

- **Sharing Code** - if you have a better explanation to someone by posting what you have done, go ahead and share with the class.  I ask that others do not copy your work, that is just wrong.  So keep learning and have fun :-)

## Help Desk / Technical Support

- Please contact the Help Desk if you are experiencing technical difficulties.  Contact me with the details later :-)
If you get disconnected during a quiz or exam, contact the Help Desk immediately by phone and then contact me.  With the problem ticket number.  We can work together on remedies.

## Email Announcement

- I want to make sure that your email works, class operations are working and the materials are ready before the course starts.  Please add my email address to your accepted email list and turn off your spam filters for my email address.

- If you need to change your email address, change your profile on the Student website.  You are responsible for keeping your email address current and correct.  I use the course email system to send out distribution emails regularly, so if your profile is not right, you will miss out on information.

- If there are any issues you can always send me an email including the telephone number and good dates and times I can call you.  I will try to call you within the times you requested.

## Facilitation of this Course

#### Announcements

- Please look at the announcements that are posted at least weekly.  I try to inform the class of any upcoming events.  These are important and can make your experience with this class a lot easier.  Some announcements have files encapsulated within the announcement, click on the link to download the announcement.

**Assignments**

- Assignments are to be written with your own words and innovative ideas.  I do not mind students referencing or building upon the lectures.  Taking someone else's work and making a couple modifications is Plagiarism.

#### Questions

- Do not be afraid to ask questions, even if you do not know the material
- If the subject matter is on topic for the current week, this is participation and you will get credit, as long as you put some substance into your question.
- Questions and Answers, the University will not allow you to acquire points.  This is what I ask:
     - If you do not know the material, please go to the Internet
     - Ask your question, do the research and then post a substantive response in the classroom
- If you still have questions, post to the graded discussion and explain your question and what you have learned from the resources you have compiled.
- Once again, you can get participation credit for asking questions :-)

#### Plagiarism / Copyright

- Please make sure all assignments and discussions are your own words and respect the copyright laws.

- **U.S. copyright laws are clear**
     - **No artifacts (documents, images, videos, etc.) can be copied without the owners permission, even if properly cited**
     - **The user must ask permission before using any materials**

#### Here are a couple references

- [Plagiarism](http://dictionary.reference.com/search?q=plagiarize)

- [Copyright](http://dictionary.reference.com/search?q=copyright)

- Academic dishonesty in an Online learning environment examples:
     - Having a tutor or friend complete a portion of your assignments
     - Having a reviewer make extensive revisions to an assignment
     - Copying work submitted by another student
     - Using information from other resources without proper citations or permissions
     
Any student who commits academic dishonesty in a course may receive zero credit for an assignment (without any opportunity for resubmission). 

#### Do not jeopardize your academic career

Finally, this is a **FUN CLASS!!!**  The range of knowledge in this course are, Students who are new to computing technology and the experts.  Don't be intimidated, we are all here to learn and share.
