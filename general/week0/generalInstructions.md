# General Instructions

This document contains general instructions on several topics explaining how this course is facilitated

## REQUIRED - git and MARKDOWN - Class please read, VERY IMPORTANT :-)

#### First and Foremost (very important) - transition to Markdown (.md)

- I will be glad to have a 1-on-1 private session with anyone willing to learn MARKDOWN, I only provided this opportunity during week 1
- So Please contact me if you are interested.  Once week 2 starts, I will no longer offer that opportunity

#### [Transition to Markdown (.md) and say good bye to Office](https://gitlab.com/bobby.estey/wikibob/-/blob/master/docs/markdown/README.md)

- All students that were handing in their assignments using git and MARKDOWN. 100% of the Class were glad they learned Markdown
- Built into many Integrated Development Environments (IDEs), e.g. IntelliJ, eclipse, Visual Studio Code
- Submissions - all you have to do is submit the link to your assignment, versus copying files, zipping up, posting, ...
- Maintainability and Configuration Management - utilizing git
- Centralize your code with Documentation and Artifacts
- Collaboration is much easier and flexible
- While some assignments are requiring Office artifacts a better approach is to take advantage of MARKDOWN
- Markdown is what Developers / Technical Writers utilize to provide documentation
- And much more...

## git account

- There are several Git Servers available, e.g. Gitlab, Bitbucket, Github, etc.  I personally prefer GITLAB
- If you do not already have a GIT Server, please contact me and I can have your environment setup for this course within 30 minutes
     - Installation of GIT on your Operating System
     - Set up a GIT repository for this course
- Having a git repository will make your life a lot easier, free configuration management, backups and you can also document utilizing MARKDOWN on YOUR PERSONAL site

#### GITLAB Instructions

- Here are the instructions to sign up for GITLAB, go here: https://gitlab.com/users/sign_up 
- Do not provide a credit card or pay anything, even after the 30 day period.  GITLAB is free
- Once you sign up, contact me and I can give you some pointers

## Biography

- I am Bobby Estey and this is my [Biography](https://gitlab.com/bobby.estey/bobby/-/blob/main/biography/biography.md)

## Best Ways to Contact Me

- Previously there was a Dr Rick (you know the man telling you not to be your parents) Commercial on answering unknown phone calls
- The commercial was removed unfortunately
- This is a funny commercial, couldn't find the 30 second commercial, if you do find the link, please send to me
- I only answer calls that I know who is calling me, SO LEAVE A MESSAGE :-)

- This is the preferred order to contact me:
     - Halo Messaging
     - Texting 775.513.7587
     - Email (robert.estey@my.gcu.edu)

- Include your name, COURSE # and Phone Number
- That's all and I will contact you back, you do not need to provide anything else, the details can be discussed later

## Introduction and Phone Call

- I am here to make you successful in this course, the best approach I have found is to have an introductory conversation on the phone
- I will cover the requirements, the announcements posted and answer any questions you have

## QUESTIONS - please ask in the discussions

- If you have questions about the course, please ask questions in the Discussion Forums and this is why:
     - I can see and respond a lot faster and your classmates can probably answer your question
     - If you ask with a DETAILED / TECHNICAL question
          - YOU GET discussion credit
          - YOUR CLASSMATE can get discussion credit
          - I can get discussion credit
     - Other classmates will learn more from your questions and can be more interactive in our course
     - and MUCH MORE, A WIN-WIN-WIN situation
- So please SHARE your question(s) with the Class :-) 

## Collaboration

- I do not mind if students COLLABORATE with each other
- LEARN TOGETHER, share your thoughts and be a team on the assignments
- Working on a problem as a team will give you better insight and ideas
- We are a team which includes ME :-)

## draw.io - Visio Alternative

- [Draw IO](https://gitlab.com/bobby.estey/wikibob/-/blob/master/docs/misc/tools/drawio.md?ref_type=heads)

## WebStorm – JavaScript Integrated Development Environment

- There is an alternative to Visual Studio Code called [WebStorm](https://www.jetbrains.com/webstorm/)
- Create an account with Jet Brains as as student for free software, **DO NOT PAY ANYTHING**
- Once the software has been installed [go here](https://www.youtube.com/watch?v=wm8WdAB64gw) for a step by step tutorial

## Course Resources

- [New Peer Support Initiative for Students via Discord](https://discord.gg/BwX2sqNhKf)
- [Tutoring and Career Services](https://ssc.gcu.edu/#/page/student-quick-access/318)
- Halo Learn: Log into your student portal to access the digital classroom associated with each of your classes and be sure to answer the first Discussion Question (DQ)
     - Please note – you may have an online class in addition to your face-to-face classes
- College Meetings: In week 2 or 3, there will be a College Kick-off for all new students hosted by each college
- College Career Launch: Beginning Week 6, Colleges provide new students with career resources specific to their program of study in their UNV-103/303 class
- ACE: If you need anything, visit an Academic & Career Excellence, or ACE, Center– there are ACE Centers spread out across campus, so stop in any one for assistance
     - ACE Centers provide academic and career support but can also get you connected with whatever resources you need
