# Final Presentation Example Template

- For the Students who have elected to use Markdown as the presentation software for all activities and milestones
- The Final Presentation should be your **OVERALL** README.md file and should be updated weekly as you are completing assignments
- This is an example of how to write up the Final Presentation

# Introduction - CST-120 Example

- The Grand Canyon University Introduction to Web Development (CST-120) course introduces the fundamental concepts and syntax of the web development languages including HTML, CSS, and JavaScript. The course focuses on foundation required to build complex dynamic web applications

# Introduction - CST-339 Example

- The Grand Canyon University Programming in Java 3 (CST-339) course introduces dynamic web applications using the Spring framework and the Java programming language. Students utilize design and programming methodologies to develop secure, high-performance, database driven applications

# Introduction - CST-391 Example

- The Grand Canyon University Introduction to JavaScript Web Application Development (CST-391) course uses current development trends, students examine several front-end and back-end frameworks used to build web applications.  Students learn how to program these modern frameworks, as well as how to integrate them using traditional enterprise technologies

## Introduction

- Enter an Introduction of what you have been performing and working on throughout the course
- Enter general discussion of the course and what you have learned during the course

## Presentation Artifacts

- Provide overall Course Project Artifacts:
     - Documentation about the Project, artifacts, images, code, etc. 
     - **General Information**, not the specifics, specifics are in your weekly Artifacts and Milestones

## Activity Assignments

|Activity|Description|
|--|--|
|Activity 1 - Link to Activity 1 Weekly Assignment|Activity 1 was ... (briefly describe) leave the details in the Activity README page|
|Activity 2 - Link to Activity 1 Weekly Assignment|Activity 2 was ... (briefly describe) leave the details in the Activity README page|
...

## Milestone Assignments

|Milestone|Description|
|--|--|
|Milestone 1 - Link to Activity 1 Weekly Assignment|Milestone 1 was ... (briefly describe) leave the details in the Milestone README page|
|Milestone 2 - Link to Activity 1 Weekly Assignment|Milestone 2 was ... (briefly describe) leave the details in the Milestone README page|
...

## Conclusion

- Provide a detailed, technical and overall conclusion
