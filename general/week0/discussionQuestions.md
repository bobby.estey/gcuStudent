# Discussion Questions and Participation

Each week this course will have 2 discussion questions.  When replying to the discussion question, this will be graded under the discussion # column of the grade book.  At the end of the week there is a participation column.  That column will be graded on how many substantive posts (replies) to the classroom during the week.  The substantive posts are not the initial discussion question but the replies to the classroom (other students or me).  Students are required to post at least 2 substantive replies PER discussion question, for this course this would be a total of 4.  Also note, all posts can only receive ONE credit per day, per graded thread.  If you make 2 posts in DQ1 on the same day, then only 1 of the replies gets credit.  HOWEVER, you can make a post in DQ1 and a post in DQ2 on the same day.

## Discussion Questions - Technical / Non-Technical

Many courses have been updated with new Discussion Questions

Certain weeks, I will post the original discussion questions which are more technical

You can answer either (Technical / Non-Technical) or both discussion questions.  This is up to your discretion

If there are any questions, please ask in the Discussion Questions so that the entire class can track our efforts

## Discussion Questions - Requirements

- The University requires students post at least 3 substantive posts and on at least 3 different days to each graded discussion.  Your first post needs to be posted no later than the third day, usually Wednesday.  Please do not miss posting to the graded discussions as all posts must be made during the discussion period -- the points can never be made up.  I just want to make sure you understand this requirement this is University policy, not mine. 

#### Discussion Questions and Participation Grades

- Each week we have graded discussion questions.  When you INITIALLY reply to the discussion question, that is graded under the discussion number column.  
- There is also a participation grade column.  The end of the week, the participation grade column is graded on how many substantive posts (replies to students or me) you posted during the week.  To receive full credit on participation, at least 2 substantive replies PER discussion question are required.  
- All posts can only receive ONE credit per day per graded thread.  If you make 2 posts in DQ1, then only 1 of the posts gets credit.  HOWEVER, you can make a post in DQ1 and a post in DQ2 on the same day and receive 1 credit for each of the DQs.

#### Your Own Work

- Discussions are to be written in your own words and innovative ideas.  Copying and pasting images and text from a referential source could be a violation of copyright laws and Plagiarism.  Read the documentation carefully, the materials are most likely copyright, respect the copyright.  If there are an artifacts you wish to share with the class, post the reference, do not copy the copyright material.

- Discussions should be no less than 5 to 6 complete sentences.  I rank the quality of the sentence and 3 substantive posts per graded discussion per week receives all points.  So please make sure you have posted substantive posts to merit all the credit.  Grades are given out weekly, please make sure you read my notes for each assignment.

- Individual discussions and participation all require the following:

     - Was the artifact, discussion, posted, Substantive???
     - Succinct, concise and to the point, stuff not fluff
     - On Topic, relevant and directly related to the material
     - Detailed, explained clearly and demonstrating an understanding of the material
     - Technical, professional, technical, not first person syntax and not personal or opinions
     - Post discussions that are Factual
     - Post at least 100 words or more per discussion, this is only a guideline
     - Was the discussion in your own words?  Do not copy work of others, cite the resources
     - If you have questions and they are substantive, post in the main classroom for credit
     - Active conversations that are substantive, will be marked substantive

**Participation is replying to others discussions and answering clearly according to the requirements.**

**If I ask a student a question, anyone can respond.  The discussions are for everyone to join in and have fun.**

- Post responses to other students, to our questions or ask questions.  Use text references, web research or life experiences which are relevant to our topics.  Do not simply post "I agree" with other students, write complete statements.
- You do NOT have to answer all of our questions.  Pick the discussions which most interest you and answer the discussions or ask followup questions.
- Debate with your classmates.  We do not all have to agree on all issues.  Debate enhances learning.
- DO ask questions when you are confused
- DO dig deep into our topics

## This table is an example of how to get full credit on both the Discussion Question and Participation

|Discussion 1|Discussion 2|
|--|--|
|Initial Response to the Discussion 1 Question|Initial Response to the Discussion 2 Question|
|Response one to the Classroom about Discussion 1 Question|Response one to the Classroom about Discussion 2 Question|
|Response two to the Classroom about Discussion 1 Question|Response two to the Classroom about Discussion 2 Question|

I hope this is clear and finally, DON'T KILL the Messenger, that would be me.  This is University Policy, not mine.  Thank You
