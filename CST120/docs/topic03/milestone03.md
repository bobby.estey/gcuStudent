# Milestone 3

## General Notes

- All Milestones require a **SINGLE - encapsulating Document, containing all artifacts** 
- The Document is either a Word Document or [Markdown Document](https://gitlab.com/bobby.estey/wikibob/-/blob/master/docs/markdown/README.md?ref_type=heads)
- Explain with technical details your images and links
- Do not just post an image or link without explanation
- Expand on the template with your own work
- ... - means expand on the Webpage with technical details

## Requirements

- Update the website from Milestone 2 with the following:
     - Add Cascading Style Sheet (CSS) formatting to the website

- Development Requirements:
     - CSS elements to support text fonts, colors, border, page background, etc.
     - CSS elements to support a transition and animation
     - CSS must be able to be reused across pages
      
## Example

### Milestone 3

#### Introduction

- This is Milestone 3 which documents the requirements, development process, screenshots and recording for this week
- Milestone 3 ...

#### References
     
- Provide Images or Links to updated pages utilizing CSS

#### Conclusion

In Conclusion ...
