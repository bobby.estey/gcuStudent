# Topic 3 - Advanced HyperText Markup Language (HTML)

## NOTE:  Please only use the Chrome Browser on this assignment

- This week we will be building on previous Topic 2 with:
     - Cascading Style Sheets (CSS) and “float” CSS attributes
     - CSS elements to support text fonts, colors, border, page background, etc.
     - CSS elements to support a transition and animation
     - CSS must be able to be reused across pages

## Examples

- [Activity 3](https://cv64.us/gcu/cst120/activity03/index.html)
- [Milestone 3](milestone03.md)

## Troubleshooting

|Issue|Solution|
|--|--|
|Audio and Video not showing up|- Safari Browsers and older Browsers might not work<br>- Use Google Chrome|
