# Milestone 6

## General Notes

- All Milestones require a **SINGLE - encapsulating Document, containing all artifacts** 
- The Document is either a Word Document or [Markdown Document](https://gitlab.com/bobby.estey/wikibob/-/blob/master/docs/markdown/README.md?ref_type=heads)
- Explain with technical details your images and links
- Do not just post an image or link without explanation
- Expand on the template with your own work
- ... - means expand on the Webpage with technical details

## Requirements

- Update the website from Milestone 5 with the following:
     - Add jQuery support to the website

- Development Requirements:
     - jQuery to support hiding and showing content
     - Bootstrap to support a menu system or navigation bar and at least two other UI components from the Bootstrap library
     - Bootstrap to support a responsive design (this can be tested using the developer tools in your browser)
      
## Example

### Milestone 6

#### Introduction

- This is Milestone 6 which documents the requirements, development process, screenshots and recording for this week
- Milestone 6 ...

#### References

- This is the Sitemap which shows ...
     - Provide Link to Sitemap
     
![Sitemap](sitemap.drawio.png)

- This is the Wireframes which shows ...
     - Provide Link to Wireframes

![Wireframes](wireframes.drawio.png)

- This is the jQuery 1 page ...
     - [jQuery 1](https://cv64.us/gcu/cst120/activity06/playjq1.html)
     
- This is the jQuery 2 page ...
     - [jQuery 2](https://cv64.us/gcu/cst120/activity06/playjq2.html)

- This is the jQuery 3 page ... 
     - This is the jQuery 3 page ...
     - [jQuery 3](https://cv64.us/gcu/cst120/activity06/playjq3.html)

- This is the Bootstrap page ... 
     - This is the Bootstrap page ...
     - [Bootstrap](https://cv64.us/gcu/cst120/activity06/index.html)
     
#### Conclusion

In Conclusion ...


