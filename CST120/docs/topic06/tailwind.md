# Tailwind - Cascade Style Sheet (CSS) / JavaScript (JS)

- Below is a snippet of how to add Tailwind to your web page, there is a Hypertext link which includes the software libraries:

```
<script src="https://cdn.tailwindcss.com"></script>
```

Below is an example of a web page with Tailwind Responsiveness:

```
<!-- Width of 16 pixel by default, 32 pixels on medium screens, and 48 pixels on large screens -->
<img class="w-16 md:w-32 lg:w-48" src="...">
```
