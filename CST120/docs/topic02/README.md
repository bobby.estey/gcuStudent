# Topic 2 - Advanced HyperText Markup Language (HTML)

## NOTE:  Please only use the Chrome Browser on this assignment

- This week we will be building on previous Topic 1 with advanced HTML elements
   - HTML Tables will be introduced
   - A Web Page developed to include audio, video and Scalable Vector Graphics (SVG)

## Examples

- [Activity 2](https://cv64.us/gcu/cst120/activity02/index.html)
- [Milestone 2](milestone02.md)

## Troubleshooting

|Issue|Solution|
|--|--|
|Audio and Video not showing up|- Safari Browsers and older Browsers might not work<br>- Use Google Chrome|
