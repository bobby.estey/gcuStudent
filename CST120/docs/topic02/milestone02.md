# Milestone 2

## General Notes

- All Milestones require a **SINGLE - encapsulating Document, containing all artifacts** 
- The Document is either a Word Document or [Markdown Document](https://gitlab.com/bobby.estey/wikibob/-/blob/master/docs/markdown/README.md?ref_type=heads)
- Explain with technical details your images and links
- Do not just post an image or link without explanation
- Expand on the template with your own work
- ... - means expand on the Webpage with technical details

## Requirements

- Update the website from Milestone 1 with the following:
     - Add two additional pages

- Development Requirements:
     - HyperText Markup Language (HTML) table element
     - HTML with anchor elements, supporting multi-page navigation
     - HTML media elements supporting audio and video playback
     - HTML lists
      
## Example

### Milestone 2

#### Introduction

- This is Milestone 2 which documents the requirements, development process, screenshots and recording for this week
- Milestone 2 ...

#### References

- This is the Sitemap which shows ...
     - Provide Link to Sitemap
     
![Milestone 1 Sitemap](sitemap.drawio.png)

- This is the Wireframes which shows ...
     - Provide Link to Wireframes

![Milestone 1 Wireframes](wireframes.drawio.png)

- This is the About Me Page ...
     - Provide Image or Link to the About Me Page
     - [Here's an example](https://cv64.us/gcu/cst120/activity01/html/biography.html)

- This is the Recording ...
     - Provide Image or Link to Recording

- The sitemap and wireframe templates, New Page 1 would be resources.html and New Page 2 would be support.html
     
- This is new Page 1 ...
     - Provide Image or Link to new Page 1
     - [Here's an example](https://cv64.us/gcu/cst120/activity02/html/resources.html)
     
- This is new Page 2 ...
     - Provide Image or Link to new Page 2
     - [Here's an example](https://cv64.us/gcu/cst120/activity02/html/support.html)

- This is the Recording ...
     - Provide Link to Recording

#### Conclusion

In Conclusion ...
