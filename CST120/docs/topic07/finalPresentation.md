# Final Presentation

## Requirements

- Update the website from Milestone 1 with the following:
     - Add Bootstrap HyperText Markup Language (HTML) to the website

- Development Requirements:
     - Develop a PowerPoint presentation that outlines the following:
          - Functional introduction to the website developed
          - Goals and design decisions for the website developed
          - Challenges encountered during the development of the website
          - Pending bugs or issues remaining in the website
          - Short (2- to 3-minute) screencast video demonstration of the working website
          - Five things learned during the course that could be leveraged in future web application projects
     - This virtual presentation will be delivered via a screencast video, 8–10 minutes long (two Loom videos may be required to meet this length)
