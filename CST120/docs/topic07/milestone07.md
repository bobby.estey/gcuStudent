# Milestone 7

## General Notes

- All Milestones require a **SINGLE - encapsulating Document, containing all artifacts** 
- The Document is either a Word Document or [Markdown Document](https://gitlab.com/bobby.estey/wikibob/-/blob/master/docs/markdown/README.md?ref_type=heads)
- Explain with technical details your images and links
- Do not just post an image or link without explanation
- Expand on the template with your own work
- ... - means expand on the Webpage with technical details

## Requirements

- Update the website from Milestone 6 with the following:
    - Update, refactor, and clean up website code as necessary
    - Updated website pages demonstrating the following technical elements:
        - Bootstrap HTML form tags to support user data entry.
        - Bootstrap HTML form processing from a server-side script:
            - For testing, post your form with a form method of POST and a form action of https://postman-echo.com/post
            - Specify a name attribute in all form elements
            - The response will be a JSON format that will contain all of the form elements posted

## Milestone 7

#### Introduction

- This is Milestone 7 which documents the requirements, development process, screenshots and recording for this week
- Milestone 7 ...

#### References

- This is the Sitemap which shows ...
     - Provide Link to Sitemap
     
![Sitemap](sitemap.drawio.png)

- This is the Wireframes which shows ...
     - Provide Link to Wireframes

![Wireframes](wireframes.drawio.png)
    
- This is Contact Professor HTML Page ...
     - Provide Link to Contact Professor HTML Page
     
- This is the Postman Image illustrating the ...
     - Provide Link to Postman Image

- This is the Recording ...
     - Provide Link to Recording

#### Conclusion

In Conclusion ...


