# Topic 05

## eXtensible Markup Language (XML) vs JavaScript Object Notation (JSON)

There are sites that perform conversions, check this site out, converting [XML to JSON](https://codebeautify.org/xmltojson)

There are many formatters / conversions on this site.

Here is a table with the XML and JSON examples next to each other:

|XML|JSON|
|--|--|
|\<ship\><br/>&nbsp;&nbsp;\<name\>USS Constellation\</name\><br/>&nbsp;&nbsp;\<designation\>CV 64\</designation\><br/>&nbsp;&nbsp;\<commissioned\>19611027\</commissioned\><br/>\</ship\>|{<br/>&nbsp;&nbsp;"ship":&nbsp;&nbsp;{<br/>&nbsp;&nbsp;&nbsp;&nbsp;"name": "USS Constellation",<br/>&nbsp;&nbsp;&nbsp;&nbsp;"designation": "CV 64",<br>&nbsp;&nbsp;&nbsp;&nbsp;"commissioned": 19611027<br/>&nbsp;&nbsp;}<br/>}|

# Links

- [Recording](topic05.mp4)
- [W3 Schools - JavaScript](https://www.w3schools.com/js/)
- [Sololearn](https://www.sololearn.com/learning)

## Examples

- [Activity 5 - helloworld.html](https://cv64.us/gcu/cst120/activity05/js/mycode.js)
- [Activity 5 - playjs.html](https://cv64.us/gcu/cst120/activity05/playjs.html)
- [Milestone 5](milestone05.md)
