# Milestone 5

## General Notes

- All Milestones require a **SINGLE - encapsulating Document, containing all artifacts** 
- The Document is either a Word Document or [Markdown Document](https://gitlab.com/bobby.estey/wikibob/-/blob/master/docs/markdown/README.md?ref_type=heads)
- Explain with technical details your images and links
- Do not just post an image or link without explanation
- Expand on the template with your own work
- ... - means expand on the Webpage with technical details

## Requirements

- Update the website from Milestone 4 with the following:
     - Add JavaScript support to the website

- Development Requirements:
     - JavaScript to support at least:
          - 2 different event handlers
          - 2 different events
          - 2 different pages
     - JavaScript must be able to be reused across pages
      
## Example

### Milestone 5

#### Introduction

- This is Milestone 5 which documents the requirements, development process, screenshots and recording for this week
- Milestone 5 ...

#### References

- This is the Conditions page which shows ...
     
![Conditions](conditions.png)

- This is the Conditions page which shows ...
     
![Loops](loops.png)

- This is the Objects page which shows ...
     
![Objects](objects.png)

- This is the Recording ...
     - Provide Link to Recording

#### Conclusion

In Conclusion ...
