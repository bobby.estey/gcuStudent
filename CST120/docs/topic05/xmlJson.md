# eXtensible Markdown Language (XML) compared to JavaScript Object Notation (JSON)

## eXtensible Markdown Language (XML) - Balanced and Well Formed

- XML is a hierarchical structure utilizing the characters "<", ">", "</" that manage the elements
- The item inside these characters are NOT TAGs, the correct term is ELEMENT, e.g  "< element >" and "</ element >"

XML is a hierarchical structure that must be well-balanced and well-formed.  For example, see the following:

```
<!-- invalid, not balanced and not well-formed, the nesting is incorrect and there are no matching elements -->
<ship>
  <name>USS Constellation</name>
  <designation>CV 64
  <commissioned>19611027
</ship>
</designation>

<!-- balanced and well-formed, the nesting is correct and there are matching elements -->
<ship>
  <name>USS Constellation</name>
  <designation>CV 64</designation>
  <commissioned>19611027</commissioned>
</ship>
```


## JavaScript Object Notation (JSON)

- JSON is a hierarchical structure utilizing the characters "{", "}", ":" that manage the keys and values, e.g  "key": "value"

For example, see the following:

```
{
  "ship": {
    "name": "USS Constellation",
    "designation": "CV 64",
    "commissioned": 19611027
  }
}
```
