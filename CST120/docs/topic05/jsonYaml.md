# JavaScript Object Notation (JSON) compared to Yet Another Markup Language (YAML)

## JavaScript Object Notation (JSON)

- JSON is a hierarchical structure utilizing the characters "{", "}", ":" that manage the keys and values, e.g  "key": "value"

For example, see the following:

```
{
  "ship": {
    "name": "USS Constellation",
    "designation": "CV 64",
    "commissioned": 19611027
  }
}
```

## Yet Another Markup Language (YAML)

- YAML is a block structure utilizing SPACES and ":" that manage the keys and values, e.g  key: value

For example, see the following:

```
ship:
    name: USS Constellation
    designation: CV 64
    commissioned: 19611027
```
