# Milestone 4

## General Notes

- All Milestones require a **SINGLE - encapsulating Document, containing all artifacts** 
- The Document is either a Word Document or [Markdown Document](https://gitlab.com/bobby.estey/wikibob/-/blob/master/docs/markdown/README.md?ref_type=heads)
- Explain with technical details your images and links
- Do not just post an image or link without explanation
- Expand on the template with your own work
- ... - means expand on the Webpage with technical details

## Requirements

- Update the website from Milestone 3 with the following:
     - Update Webpages with Cascading Style Sheet (CSS) Flexbox
     - Ensure the Webpages demonstrate proper row wrapping and control of margins
     - Add at least one custom font from [font.google.com](https://fonts.google.com)
      
## Example

### Milestone 4

#### Introduction

- This is Milestone 4 which documents the requirements, development process, screenshots and recording for this week
- Milestone 4 ...

#### References
     
- Provide Images or Links of updated pages utilizing CSS Flexbox and Custom Fonts

- This is the Recording ...
     - Provide Link to Recording

#### Conclusion

In Conclusion ...
