# Activity 4

## Requirements

- Development Requirements is JavaScript implementing the following:
     - Getting Started (the basics)
     - Operations and Strings
     - Decision Making
     - Loops
     - Functions
      
## Example

### Activity 4

#### Overview Section

- This is Activity 4 which documents the requirements, development process, screenshots and recording for this week
- Activity 4 ...

#### References

###### helloworld.html

- This is the image of the Alert display

![Alert](alert.png)

- This is the image displaying Before and After running JavaScript

![Before / After Running](beforeAfterRunning.png)

###### playjs.html

- This is the image displaying JavaScript being implemented, e.g. operations, strings, decision making, loops, functions, etc.
- Using Google Chrome Development Tools select Inpect and render the Console output displays the following:

![Implementation Console 1](console1.png)
![Implementation Console 2](console2.png)

#### Conclusion

In Conclusion ...


