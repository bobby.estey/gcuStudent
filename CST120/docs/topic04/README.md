# Topic 4 - Basic JavaScript

- This week we will be learning Basic JavaScript as follows:
     - Getting Started (the basics)
     - Operations and Strings
     - Decision Making
     - Loops
     - Functions

## Examples

- [Activity 4 - helloworld.html](https://cv64.us/gcu/cst120/activity04/helloworld.html)
- [Activity 4 - playjs.html](https://cv64.us/gcu/cst120/activity04/playjs.html)
- [Activity 4 - Presentation](activity04.md)
- [Milestone 4](milestone04.md)
