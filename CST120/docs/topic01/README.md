# Topic 1 - HyperText Markup Language (HTML)

- This week we are learning HTML which is the framework and content of a Web Page
- Navigate to the Syllabus -> Class Materials and please read the instructions on what is required for this week

## Tools / Resources

- Visual Studio Code (VSC) Integrated Development Environment (IDE) is the tool we will utilize in this course
     - Download and Install VSC that is documented in the Class Materials
- [W3Schools](https://www.w3schools.com/html/) - HTML Tutorials will be discussed this week

## NOTE:  Git and Markdown (highly suggested, NOT REQUIRED to use Markdown versus Office)

- [Markdown Link](https://gitlab.com/bobby.estey/wikibob/-/blob/master/docs/markdown/README.md?ref_type=heads)

#### If the Instructions are confusing, contact me, preferably by Text and I will walk you through all of this

- I would rather you contact me and we can go through this correctly the first time :-)

#### Directory Setup

- If you are utilizing Git and Markdown, I would highly suggest set up the architecture as follows:

|Directory|Description|
|--|--|
|c:\git\cst120|top level directory|
|c:\git\cst120\activities|top level activities directory|
|c:\git\cst120\activities\activity1..n|each activity will be inside the specific activity topic, e.g. activity1, activity2, ...|
|c:\git\cst120\milestones|top level milestones directory|
|c:\git\cst120\milestones\milestone1..n|each milestone will be inside the specific milestone, e.g. milestone1, milestone2, ...|

```
                             c:\git\cst120
                                   |
            ------------------------------------------------
            activities                            milestones
                |                                      |
---------------------------------     ------------------------------------
activity1 activity2 ... activity7     milestone1 milestone2 ... milestone7
```

## Examples:

- [Activity 1 - Part 1](https://cv64.us/gcu/cst120/activity01/part1/test.html)
- [Activity 1 - Part 2](https://cv64.us/gcu/cst120/activity01/part2/)
- [Milestone 1](milestone01.md)
