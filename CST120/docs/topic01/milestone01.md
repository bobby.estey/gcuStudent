# Milestone 1

## General Notes

- All Milestones require a **SINGLE - encapsulating Document, containing all artifacts** 
- The Document is either a Word Document or [Markdown Document](https://gitlab.com/bobby.estey/wikibob/-/blob/master/docs/markdown/README.md?ref_type=heads)
- Explain with technical details your images and links
- Do not just post an image or link without explanation
- Expand on the template with your own work
- ... - means expand on the Webpage with technical details

## Requirements

- Provide a website design consisting of the following:
     - User Interface Sitemap
     - User Interface Wireframes design
     - About Me Page in the Overview Section
     - Recording

- Development Requirements:
     - HyperText Markup Language (HTML) supporting basic page structure
     - HTML with formatting, fonts, colors, etc.
     - HTML images including "float" and "height" properties, controlling text wrapping
     - HTML lists
      
## Example

### Milestone 1

#### Introduction

- This is Milestone 1 which documents the requirements, development process, screenshots and recording for this week
- Milestone 1 ...

#### References

- This is the Sitemap which shows ...
     - Provide Image or Link to Sitemap
     
![Milestone 1 Sitemap](sitemap.drawio.png)

- This is the Wireframes which shows ...
     - Provide Image or Link to Wireframes

![Milestone 1 Wireframes](wireframes.drawio.png)

- This is the About Me Page ...
     - Provide Image or Link to the About Me Page
     - [Here's an example](https://cv64.us/gcu/cst120/activity01/html/biography.html)

- This is the Recording ...
     - Provide Link to Recording

#### Conclusion

In Conclusion ...
