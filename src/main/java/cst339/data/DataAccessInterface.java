package cst339.data;

import java.util.List;

import cst339.data.entity.OrderEntity;

public interface DataAccessInterface<T> {

	public List<T> findAll();

	public T findById(int id);

	public boolean create(OrderEntity order);

	public boolean update(T t);

	public boolean delete(T t);
}
