# Estey Genealogy Mobile Application

## Background

Several years ago I wanted to learn about mobile development and the best approach was to develop something of personal interest.  I wrote an Android Genealogy mobile application that interfaced with a SQL Lite Database.

I have always been interested in my Family Genealogy.  I met Willis Estey - Rickert a long time ago, whose Maternal side was Estey.

Willis kept a document of names, dates, miscellaneous information and family tree information.  I converted all this information into a database.  Then I wrote an Android application to interface with the database, rendering the data and displaying the family tree. 

I am currently rewriting the Genealogy Application from an Android Application to an Angular Web Application that will render on a Mobile device.  This is called Responsiveness, in which an application can be rendered on any type of device and cross platform support.

#### Recordings

There are Recordings that cover all the capabilities of the application which are not documented here due to confidential information.  Thus screenshots are utilized.

#### Releases

Below is some of the releases that were generated, the initial release is Mom and the latest is Mike.

![Estey Genealogy Releases](esteyGenealogyReleases.png)

## Application

#### Login Screen

This is the login screen which asks for a Username and Password.

![Login Screen](loginScreen.png)

#### Main Screen

This is the main screen of the application with various artifacts:

- The title bar **Estey Genealogy** 
- Home Icon - returns the application to the Main Screen
- cv64 Icon navigates to the cv64.us website
- [...]](menuItems.png) - navigates to other websites and release information
- Coat of Arms - information on the Estey Coat of Arms
- [Genealogy](genealogySearch.png) - navigates to the User Interface Search engine
- [CSV Text](csvTextScreen.png) - navigates to Comma Separated Value Search engine
- [About](aboutScreen.png) - the about me page
- Family Images that navigates to specific ancestors

#### About Screen

This is the about screen of the application with information about the application.  The different releases are located at the bottom.

![About Screen](aboutScreen.png)

#### Menu Items

This is the menu items screen of the application.  Shows links to various artifacts (recordings, images) of Estey, MI and Estey City, NM and The current Version

![Menu Items](menuItems.png)

#### Genealogy Search

This is the Genealogy Search screen of the application.  This screen has various information about the individual.

- Parent - navigates to the Parent page
- Children - navigates to the Children page
- Search - performs a search and displays the information on the search
- Photos - future use - displays photos of the individual

![Genealogy Search Screen](genealogySearch.png)

#### Childrens Page

This is the Childrens Page screen of the application.  This screen displays all the Children of the individual.  For instance **CHRISTOPHER ESTEY (ID - G)** is the parent of **JEFFREY (ID - GA)** through **ELIZABETH (ID - GH)**

![Childrens Page](childrensPage.png)

###### Genealogy Mapping

This is the Genealogy Mapping of the application.

The first example shows **Roland - ID (5000)** had 4 children.  Each child is given a letter in order of birth.  **Robert - ID (5000D)** is the forth child and had 5 children **IDs 5000DA through 5000DE**

![Genealogy Mapping 1](genealogy1.png)

![Genealogy Mapping 2](genealogy2.png)

#### Comma Separated Value (CSV) Search Engine

This is the Comma Separated Value (CSV) Search Engine of the application.

![CSV Text Screen](csvTextScreen.png)

## Conclusion

This document is the initial release of what is being planned for the future of the Estey Genealogy application.
