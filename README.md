# Repository for GCU Artifacts

# Links

|Folders|Description|
|--|--|
|docs|documentation content|
|solutions|solutions to the assignments|

- [CST120](https://gitlab.com/bobby.estey/gcuStudent/-/tree/main/CST120?ref_type=heads)
- [CST339](https://gitlab.com/bobby.estey/gcuStudent/-/tree/main/CST339?ref_type=heads)
- [CST391](https://gitlab.com/bobby.estey/gcuStudent/-/tree/main/CST391?ref_type=heads)
- [SDD630](https://gitlab.com/bobby.estey/gcuStudent/-/tree/main/SDD630?ref_type=heads)
- [SDD680](https://gitlab.com/bobby.estey/gcuStudent/-/tree/main/SDD680?ref_type=heads)
