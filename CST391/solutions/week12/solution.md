# Week 12 - Scripting Solution

# Question

Given a set of data ['jan', 'feb', 'mar', 'apr', 'may', 'jun', 'jul', 'aug', 'sep', 'oct', 'nov', 'dec'] write two functions that utilize JavaScripts map and filter methods.  In one function, generate a list of capitalized words.  In the other function, return only the words that begin with the letter "j".  Create your code in www.CodePen.io  Share the link in your answer.

# Solution 1 (Old Approach - without map / filter)


```
<script>
    let months = ['jan', 'feb', 'mar', 'apr', 'may', 'jun', 'jul', 'aug', 'sep', 'oct', 'nov', 'dec'];
    let capitalMonths = months;
    let jMonths = [];


    function capitalizeFunction() {
      for (let i = 0; i < months.length; i++) { 
        capitalMonths[i] = months[i].toUpperCase();
      }
    }
  
    function findJMonthsFunction() {
      for (let i = 0; i < months.length; i++) {
        if (months[i].substring(0,1) == "j" || months[i].substring(0,1) == "J") {
          jMonths.push(months[i]);
        }
      }
    }
    console.log(months[0].substring(0,1));
  
    function outputArrayToDocument(input) {
      document.write("<p>");
      for (let i = 0; i < input.length; i++) {
        document.write(input[i] + ", ");
      }
      document.write("</p>");
    }

    document.write("<b>Months:</b>");
    outputArrayToDocument(months);
    document.write("<br>");

    findJMonthsFunction();
    capitalizeFunction();
    
    document.write("<b>Capitalized Months:</b>");
    outputArrayToDocument(capitalMonths);
    document.write("<br>");
    
    document.write("<b>J Months:</b>");
    outputArrayToDocument(jMonths);
    document.write("<br>");
  </script>
  ```
  
  # Solution 2 (New Approach - with map / filter)
  
  
  ```
  // Uppercase Month function
  uppercaseMonth = (monthList) => {
    monthList.map((str) => {
      let row = str.charAt(0).toUpperCase() + str.slice(1);
      let paragraphElement = document.createElement("p");
      paragraphElement.innerText = `${row} `;
      calendarApplication.appendChild(paragraphElement);
      return data;
    });
  }
  
  // Return J Months function
  jMonths = (jMonthList) => {
    let row = jMonthList.filter((month) => {
      let monthFirstCharacter = month.charAt(0);
      if (monthFirstCharacter === "j") {
        return month;
      }
    })
  }
      
  ```
  
  