"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.Track = void 0;

var _classCallCheck2 = _interopRequireDefault(require("@babel/runtime/helpers/classCallCheck"));

var _createClass2 = _interopRequireDefault(require("@babel/runtime/helpers/createClass"));

var _defineProperty2 = _interopRequireDefault(require("@babel/runtime/helpers/defineProperty"));

var Track =
/*#__PURE__*/
function () {
  function Track(id, number, title, lyrics, video) {
    (0, _classCallCheck2.default)(this, Track);
    (0, _defineProperty2.default)(this, "id", -1);
    (0, _defineProperty2.default)(this, "number", 0);
    (0, _defineProperty2.default)(this, "title", "");
    (0, _defineProperty2.default)(this, "lyrics", "");
    (0, _defineProperty2.default)(this, "video", "");
    this.id = id;
    this.number = number;
    this.title = title;
    this.lyrics = lyrics;
    this.video = video;
  }

  (0, _createClass2.default)(Track, [{
    key: "Id",
    get: function get() {
      return this.id;
    },
    set: function set(id) {
      this.id = id;
    }
  }, {
    key: "Number",
    get: function get() {
      return this.number;
    },
    set: function set(number) {
      this.number = number;
    }
  }, {
    key: "Title",
    get: function get() {
      return this.title;
    },
    set: function set(title) {
      this.title = title;
    }
  }, {
    key: "Lyrics",
    get: function get() {
      return this.lyrics;
    },
    set: function set(lyrics) {
      this.lyrics = lyrics;
    }
  }, {
    key: "Video",
    get: function get() {
      return this.video;
    },
    set: function set(value) {
      this.video = value;
    }
  }]);
  return Track;
}();

exports.Track = Track;
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uLy4uL2FwcC9tb2RlbHMvVHJhY2sudHMiXSwibmFtZXMiOlsiVHJhY2siLCJpZCIsIm51bWJlciIsInRpdGxlIiwibHlyaWNzIiwidmlkZW8iLCJ2YWx1ZSJdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7Ozs7Ozs7O0lBQWFBLEs7OztBQVFULGlCQUFZQyxFQUFaLEVBQXVCQyxNQUF2QixFQUFzQ0MsS0FBdEMsRUFBb0RDLE1BQXBELEVBQW1FQyxLQUFuRSxFQUNBO0FBQUE7QUFBQSw4Q0FQcUIsQ0FBQyxDQU90QjtBQUFBLGtEQU55QixDQU16QjtBQUFBLGlEQUx3QixFQUt4QjtBQUFBLGtEQUp5QixFQUl6QjtBQUFBLGlEQUh3QixFQUd4QjtBQUNJLFNBQUtKLEVBQUwsR0FBVUEsRUFBVjtBQUNBLFNBQUtDLE1BQUwsR0FBY0EsTUFBZDtBQUNBLFNBQUtDLEtBQUwsR0FBYUEsS0FBYjtBQUNBLFNBQUtDLE1BQUwsR0FBY0EsTUFBZDtBQUNBLFNBQUtDLEtBQUwsR0FBYUEsS0FBYjtBQUNIOzs7O3dCQUdEO0FBQ0ksYUFBTyxLQUFLSixFQUFaO0FBQ0gsSztzQkFDTUEsRSxFQUNQO0FBQ0ksV0FBS0EsRUFBTCxHQUFVQSxFQUFWO0FBQ0g7Ozt3QkFHRDtBQUNJLGFBQU8sS0FBS0MsTUFBWjtBQUNILEs7c0JBQ1VBLE0sRUFDWDtBQUNJLFdBQUtBLE1BQUwsR0FBY0EsTUFBZDtBQUNIOzs7d0JBR0Q7QUFDSSxhQUFPLEtBQUtDLEtBQVo7QUFDSCxLO3NCQUNTQSxLLEVBQ1Y7QUFDSSxXQUFLQSxLQUFMLEdBQWFBLEtBQWI7QUFDSDs7O3dCQUdEO0FBQ0ksYUFBTyxLQUFLQyxNQUFaO0FBQ0gsSztzQkFDVUEsTSxFQUNYO0FBQ0ksV0FBS0EsTUFBTCxHQUFjQSxNQUFkO0FBQ0g7Ozt3QkFHRDtBQUNJLGFBQU8sS0FBS0MsS0FBWjtBQUNILEs7c0JBRWdCQyxLLEVBQ2pCO0FBQ0ksV0FBS0QsS0FBTCxHQUFhQyxLQUFiO0FBQ0giLCJzb3VyY2VzQ29udGVudCI6WyJleHBvcnQgY2xhc3MgVHJhY2tcbntcbiAgICBwcml2YXRlIGlkOiBudW1iZXIgPSAtMTtcbiAgICBwcml2YXRlIG51bWJlcjogbnVtYmVyID0gMDtcbiAgICBwcml2YXRlIHRpdGxlOiBzdHJpbmcgPSBcIlwiO1xuICAgIHByaXZhdGUgbHlyaWNzOiBzdHJpbmcgPSBcIlwiO1xuICAgIHByaXZhdGUgdmlkZW86IHN0cmluZyA9IFwiXCI7XG4gXG4gICAgY29uc3RydWN0b3IoaWQ6bnVtYmVyLCBudW1iZXI6bnVtYmVyLCB0aXRsZTpzdHJpbmcsIGx5cmljczpzdHJpbmcsIHZpZGVvOnN0cmluZylcbiAgICB7XG4gICAgICAgIHRoaXMuaWQgPSBpZDtcbiAgICAgICAgdGhpcy5udW1iZXIgPSBudW1iZXI7XG4gICAgICAgIHRoaXMudGl0bGUgPSB0aXRsZTtcbiAgICAgICAgdGhpcy5seXJpY3MgPSBseXJpY3M7XG4gICAgICAgIHRoaXMudmlkZW8gPSB2aWRlbztcbiAgICB9XG5cbiAgICBnZXQgSWQoKTpudW1iZXJcbiAgICB7XG4gICAgICAgIHJldHVybiB0aGlzLmlkO1xuICAgIH1cbiAgICBzZXQgSWQoaWQ6bnVtYmVyKVxuICAgIHtcbiAgICAgICAgdGhpcy5pZCA9IGlkO1xuICAgIH1cblxuICAgIGdldCBOdW1iZXIoKTpudW1iZXJcbiAgICB7XG4gICAgICAgIHJldHVybiB0aGlzLm51bWJlcjtcbiAgICB9XG4gICAgc2V0IE51bWJlcihudW1iZXI6bnVtYmVyKVxuICAgIHtcbiAgICAgICAgdGhpcy5udW1iZXIgPSBudW1iZXI7XG4gICAgfVxuXG4gICAgZ2V0IFRpdGxlKCk6c3RyaW5nXG4gICAge1xuICAgICAgICByZXR1cm4gdGhpcy50aXRsZTtcbiAgICB9XG4gICAgc2V0IFRpdGxlKHRpdGxlOnN0cmluZylcbiAgICB7XG4gICAgICAgIHRoaXMudGl0bGUgPSB0aXRsZTtcbiAgICB9XG5cbiAgICBnZXQgTHlyaWNzKCk6c3RyaW5nXG4gICAge1xuICAgICAgICByZXR1cm4gdGhpcy5seXJpY3M7XG4gICAgfVxuICAgIHNldCBMeXJpY3MobHlyaWNzOnN0cmluZylcbiAgICB7XG4gICAgICAgIHRoaXMubHlyaWNzID0gbHlyaWNzO1xuICAgIH1cblxuICAgIHB1YmxpYyBnZXQgVmlkZW8oKTogc3RyaW5nIFxuICAgIHtcbiAgICAgICAgcmV0dXJuIHRoaXMudmlkZW87XG4gICAgfVxuXG4gICAgcHVibGljIHNldCBWaWRlbyh2YWx1ZTogc3RyaW5nKSBcbiAgICB7XG4gICAgICAgIHRoaXMudmlkZW8gPSB2YWx1ZTtcbiAgICB9XG59Il19