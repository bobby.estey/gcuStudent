# Week 5 Artifacts

- The Sites which are obsolete have strike through on the links, I have updated the instructions to follow for the recent information:

## Activities

- [Activity 5](activity5.md)

## React Main Concepts

- Read all the sections found under "React Main Concepts" on the ReactJS Website:
     - ~~https://reactjs.org/docs/hello-world.html~~
     - https://react.dev
     
## Activity 5 Guide

- [Activity 5 Guide](https://mygcuedu6961.sharepoint.com/:w:/r/sites/CSETGuides/CST/CST-391/CST-391-RS-Activity5Guide.docx?d=wf69fcb753c1b4e9185ace1477c6c09ba&csf=1&web=1&e=Hwdqz7)

