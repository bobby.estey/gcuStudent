import React from 'react';
import { ReactDOM } from 'react-dom';

const App = () => {
	return (
		<div>This is the app!</div>
	)
}

ReactDOM.render(<App />, document.querySelector("#root"));
