# Activity 5

## Test Links

- http://localhost:3000
- https://getbootstrap.com/docs/5.3/components/card/
- https://www.bootstrapcdn.com/

## Activity 5 Commands

```
mkdir activity5
cd activity5
npx create-react-app music
cd music/src
rm -rf *
cp ../../../../docs/topic05/index.js .
npm i bootstrap
```

## Activity 5 Example

![Activity 5](activity5.png)

## Deliverables

- Cover Page
- Executive Summary
- Captioned screenshots with explanations of each page
- Conclusion

## Troubleshooting

|Issue|Solution|
|--|--|
|||
