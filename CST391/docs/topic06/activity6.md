# Activity 6

## Activity 6 Commands

```
cd activity6

```

## Test Links

- http://localhost:3000

## Deliverables

- Cover Page
- Executive Summary
- Captioned screenshots with explanations of each page
     - Take screenshots of the application you created
     - Be sure to show the various features that were illustrated in this lesson
     - Write a one-paragraph summary of the new features that have been added
     - Define new terminology that was used in the lesson
- Conclusion

## Troubleshooting

|Issue|Solution|
|--|--|
||||
