# Week 6 Artifacts

- The Sites which are obsolete have strike through on the links, I have updated the instructions to follow for the recent information:

## Activities

- [Activity 6](activity6.md)

## React Main Concepts

- Read all the sections found under "React Main Concepts" on the ReactJS Website:
     - ~~https://reactjs.org/docs/state-and-lifecycle.html~~
     - https://react.dev/
     
## Activity 6 Guide

- [Activity 6 Guide](https://mygcuedu6961.sharepoint.com/:w:/r/sites/CSETGuides/CST/CST-391/CST-391-RS-Activity6Guide.docx?d=w915f8462e08c453ba9f643b324a96150&csf=1&web=1&e=d6mObF)

