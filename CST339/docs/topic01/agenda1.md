# Topic 1 Agenda - 1

## Please turn your Microphones up - sometimes I can't hear Students when they have questions, Thank You

## Announcements

- Please read the weekly announcements
- Markdown Links, please send the actual link to the Markdown file, not directory, for example:  

```
<path>/activities/activity/README.md
```

## Welcome to Class - Programming in Java III

- Syllabus
     - Class Resources
     - Use any Integrated Development Environment (IDE) you are comfortable using, e.g. eclipse, IntelliJ, Visual Studio Code
     - Not all Resources are required to be installed during week 1
     - Download the [latest version of Java](https://en.wikipedia.org/wiki/Java_version_history)

- Week 0 Announcements

- Please read the weekly announcements
- Participation Requirements

- Collaborative Learning Community (CLC)
     - I allow up to 8 people per group in the CLC
     - Please select your teammates and add to your group

- Markdown
     - Final Presentation Example Template
     - [Markdown Tutorial](https://gitlab.com/bobby.estey/wikibob/-/blob/master/docs/markdown/README.md)
     - Markdown Links, please send the actual link to the Markdown file, not directory
     
- Git Notes
     - [Gitlab](https://gitlab.com/bobby.estey/wikibob/-/blob/master/docs/markdown/gitlab/README.md)
     - Github - send invite to bobby.estey@gmail.com (not University email)

## Questions

- Git Server
- Markdown
- Activity 1
- Milestone 1

- Students remaining can use this time for Office Hours
