# Topic 1 Agenda - 2

## Please turn your Microphones up - sometimes I can't hear Students when they have questions, Thank You

## Announcements

- Please read the weekly announcements
- Markdown Links, please send the actual link to the Markdown file, not directory, for example:  

```
<path>/activities/activity/README.md
```

## Links

- [Spring Projects](https://spring.io/projects)
- [Spring Framework](https://gitlab.com/bobby.estey/wikibob/-/blob/master/docs/spring/framework/README.md)
- [Maven](https://gitlab.com/bobby.estey/wikibob/-/blob/master/docs/maven/README.md?ref_type=heads)

## Topic 1 Documentation

- [Start Spring IO](https://start.spring.io/)
- [Create Spring Project](createSpringProject.png)
- [Activity 1](activity01.md)

## Topic 1 Solution

- [Code](https://gitlab.com/bobby.estey/gcuStudent/-/tree/main/CST339/solutions/topic01)
- [Topic 1 Solution](topic1Solution.png)

## Questions

- Git Server
- Markdown
- Activity 1
- Milestone 1

- Students remaining can use this time for Office Hours
