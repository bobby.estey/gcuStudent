# CST339 - Activity 5

- Date:  2025 January 28
- Author:  Robert Estey

## Introduction

- This activity will provide the following:
     - Evaluate the benefits and drawbacks of various mobile application frameworks
     - Formulate a recommendation for selecting a mobile application framework

## MongoDB Configuration




## Screenshots

#### Part 1:  Creating Data Services Using Spring Data MongoDB

- This is a screenshot of the Orders Page

![Orders Page](ordersPage.png)

#### Part 2:  Adding New Queries in the MongoDB Repository

- This is a screenshot of the Orders Page

![Orders Page](ordersPage.png)

## Research Questions - located in Activity Guide

- Post the Questions and Answers

## Conclusion

- Place a Conclusion here
