# Topic 2 Agenda - 6

## Please turn your Microphones up - sometimes I can't hear Students when they have questions, Thank You

## Announcements

- Please read the weekly announcements
- Markdown Links, please send the actual link to the Markdown file, not directory, for example:  

```
<path>/activities/activity/README.md
```

## Links

#### - What is Postman?

- Allows developers to easily create and share API requests and collections
- Automate testing, mock APIs, and monitor performance

#### Postman Main Website Note

- Depending on what mode your cookies are set, the following website will appear differently.
     - If you have an account and signed in with cookies, the site will look like a Development Site
     - If you are new or **SIGNED IN WITH COGNITO** the site will look like a standard website
- [Postman](https://www.postman.com/)
- [Postman Git Repository](https://github.com/postmanlabs/.github/blob/main/profile/README.md)

## Demo Topic 3 using Postman

- Start SpringBoot Topic 3 Server
- Start Postman

#### End Point Tests

|Endpoint|Description|
|--|--|
|http://localhost:6464/service/getxml|Returns Database Values in XML|
|http://localhost:6464/service/getjson|Returns Database Values in JSON|

## Questions

- Git Server
- Markdown
- Activity 2
- Milestone 2
     
- Students remaining can use this time for Office Hours
