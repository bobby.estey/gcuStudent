# Topic 2 Agenda - 2

## Please turn your Microphones up - sometimes I can't hear Students when they have questions, Thank You

## Announcements

- Please read the weekly announcements
- Markdown Links, please send the actual link to the Markdown file, not directory, for example:  

```
<path>/activities/activity/README.md
```

## Creating Forms with Data Validation Using Spring MVC

- [Spring Model, View, Controller (MVC) Example](https://spring.io/guides/gs/serving-web-content)

```mermaid
architecture-beta
    group mvc(cloud)[MVC]

    service model(database)[Model] in mvc
    service view(disk)[View] in mvc
    service controller(controller)[Controller] in mvc

    model:R -- L:controller
    model:B -- T:view
    controller:B -- T:view
```
## Questions

- Git Server
- Markdown
- Activity 2
- Milestone 2
     
- Students remaining can use this time for Office Hours
