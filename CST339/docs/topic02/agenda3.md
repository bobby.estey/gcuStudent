# Topic 2 Agenda - 3

## Please turn your Microphones up - sometimes I can't hear Students when they have questions, Thank You

## Announcements

- Please read the weekly announcements
- Markdown Links, please send the actual link to the Markdown file, not directory, for example:  

```
<path>/activities/activity/README.md
```

## Creating Layouts Using Thymeleaf and Bootstrap

## UI Links

- [Bootstrap](https://getbootstrap.com/)
- [Material UI](https://mui.com/)
- [AG Grid](https://www.ag-grid.com/example/)

## HTML Inspector Link

- [HTML Inspector](https://gitlab.com/bobby.estey/wikibob/-/blob/master/docs/webBrowsers/devtools/googleDevtools.md?ref_type=heads)

## Questions

- Git Server
- Markdown
- Activity 2
- Milestone 2
     
- Students remaining can use this time for Office Hours
