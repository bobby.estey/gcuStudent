# Topic 2 Agenda - 5

## Please turn your Microphones up - sometimes I can't hear Students when they have questions, Thank You

## Announcements

- Please read the weekly announcements
- Markdown Links, please send the actual link to the Markdown file, not directory, for example:  

```
<path>/activities/activity/README.md
```

## Questions

- Git Server
- Markdown
- Activity 2
- Milestone 2
     
- Students remaining can use this time for Office Hours
