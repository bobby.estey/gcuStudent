# Topic 3 Agenda - 4

## Please turn your Microphones up - sometimes I can't hear Students when they have questions, Thank You

## Announcements

- Please read the weekly announcements
- Markdown Links, please send the actual link to the Markdown file, not directory, for example:  

```
<path>/activities/activity/README.md
```

## Preparation to Topic 4

#### pom.xml update

- spring-boot-starter-data-jdbc - Spring Boot application with JDBC libraries
- mysql-connector-java - Java driver implementation of the MySQL protocol
- jasypt-spring-boot-starter - Provides Encryption support for property sources, e.g. Databases in Spring Boot Applications

```xml
<dependency>
	<groupId>org.springframework.boot</groupId>
	<artifactId>spring-boot-starter-data-jdbc</artifactId>
</dependency>
<dependency>
	<groupId>mysql</groupId>
	<artifactId>mysql-connector-java</artifactId>
	<version>8.0.33</version>
</dependency>
<dependency>
	<groupId>com.github.ulisesbocchio</groupId>
	<artifactId>jasypt-spring-boot-starter</artifactId>
	<version>2.1.0</version>
</dependency>
```

#### Application Properties - Database Settings 

- [application.properties](../../../src/main/resources/application.properties)

#### MAMP / MySQL Workbench / Jasypt - Installation

- [MAMP](https://www.mamp.info/en/downloads/)
- [MySQL Workbench](https://dev.mysql.com/downloads/workbench/)
- [Jasypt Encryption / Decryption](https://www.devglan.com/online-tools/jasypt-online-encryption-decryption)

## Docker (Not Required - But Recommended)

- [Docker Web Site](https://docs.docker.com/)
- [What is Docker](https://docs.docker.com/get-started/docker-overview/)
- [Continuous Integration and Continuous Delivery (CI/CD)](https://www.mabl.com/blog/what-is-cicd)
- [WikiBob Docker](https://gitlab.com/bobby.estey/wikibob/-/blob/master/docs/docker/README.md)

#### Quick Docker Demo

- Open Up Docker Desktop
- Open Up MySQL Workbench
- Start MySQL in Docker Desktop
- View MySQL status in MySQL Workbench
