# Topic 4 Agenda - 4

## Please turn your Microphones up - sometimes I can't hear Students when they have questions, Thank You

## Announcements

- Please read the weekly announcements
- Markdown Links, please send the actual link to the Markdown file, not directory, for example:  

```
<path>/activities/activity/README.md
```

## Links

- [MySQL Installation](https://gitlab.com/bobby.estey/wikibob/-/blob/master/docs/database/mysql/README.md?ref_type=heads)
- [Docker Installation](https://gitlab.com/bobby.estey/wikibob/-/blob/master/docs/docker/README.md?ref_type=heads)

## Future

- Amazon Web Services
