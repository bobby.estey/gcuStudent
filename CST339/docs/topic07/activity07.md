# CST339 - Activity 7

- Date:  2025 January 28
- Author:  Robert Estey

## Introduction

- This activity will provide the following:
     - Compare tools required to support mobile application development
     - Choose technologies that are appropriate for the current job market and adhere to sound ethical practices
     - Compare the skill set of a web developer to a mobile application developer
     - Create job requisitions appropriate for junior and senior mobile application developer positions
     - Evaluate proper candidates for a particular role

## Configuration




## Screenshots

#### Part 1:  Building a Web Application That Consumes Microservices

- This is a screenshot of the Orders Page

![Orders Page](ordersPage.png)

#### Part 2:  Integrating a REST Service Registry and Discovery Service

- This is a screenshot of the Orders Page

![Orders Page](ordersPage.png)

## Research Questions - located in Activity Guide

- Post the Questions and Answers

## Conclusion

- Place a Conclusion here
