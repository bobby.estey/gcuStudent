# Topic 7 - Hiring a Mobile Development Team

- Compare tools required to support mobile application development.
- Choose technologies that are appropriate for the current job market and adhere to sound ethical practices.
- Compare the skill set of a web developer to a mobile application developer.
- Create job requisitions appropriate for junior and senior mobile application developer positions.
- Evaluate proper candidates for a particular role.

## Agendas

- [Agenda 1](agenda1.md)
- [Agenda 2](agenda2.md)
- [Agenda 3](agenda3.md)
- [Agenda 4](agenda4.md)

## Examples:

- [Activity 7](activity07.md)
- [Milestone 7](milestone07.md)
