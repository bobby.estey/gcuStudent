# CST339 - Activity 6

- Date:  2025 January 28
- Author:  Robert Estey

## Introduction

- This activity will provide the following:
     - Compare and contrast benefits of data storage technologies used in back-end services
     - Identify and select cloud services that enhance mobile applications, including, but not limited to:
          - Artificial intelligence, voice recognition, data storage, and location-aware services
     - Recommend a data storage solution for a mobile application based on its features

## Configuration




## Screenshots

#### Part 1:  Securing a Web Application Using an In-Memory Datastore

- This is a screenshot of the Orders Page

![Orders Page](ordersPage.png)

#### Part 2:  Securing a Web Application Using a Database

- This is a screenshot of the Orders Page

![Orders Page](ordersPage.png)

#### Part 3:  Securing REST APIs Using Basic HTTP Authentication

- This is a screenshot of the Orders Page

![Orders Page](ordersPage.png)

#### Part 4:  Securing REST APIs Using OAuth2 Authentication

- This is a screenshot of the Orders Page

![Orders Page](ordersPage.png)

## Research Questions - located in Activity Guide

- Post the Questions and Answers

## Conclusion

- Place a Conclusion here
