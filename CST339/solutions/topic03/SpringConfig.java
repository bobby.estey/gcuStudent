package cst339;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.context.annotation.SessionScope;

import cst339.business.OrdersBusinessService;
import cst339.business.OrdersBusinessServiceInterface;

@Configuration
public class SpringConfig {

	@Bean(name="ordersBusinessServce", initMethod="init", destroyMethod="destroy")
//	@Scope(value="prototype", proxyMode=ScopedProxyMode.TARGET_CLASS)
//	@RequestScope
	@SessionScope
	public OrdersBusinessServiceInterface getOrdersBusiness() {
		
		return new OrdersBusinessService();
		
//		return new AnotherOrdersBusinessService();
	}
}
